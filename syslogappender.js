// eslint-disable-next-line no-console
const consoleLog = console.log.bind(console);

function consoleAppender(layout, timezoneOffset) {
  return (loggingEvent) => {
    console.log("ログ出力前:", loggingEvent);
    consoleLog("[hogehoge]" + layout(loggingEvent, timezoneOffset));
  };
}

function configure(config, layouts) {
  console.log("syslogappender: configure", config, layouts);
  let layout = layouts.colouredLayout;
  if (config.layout) {
    layout = layouts.layout(config.layout.type, config.layout);
  }
  return consoleAppender(layout, config.timezoneOffset);
}

module.exports.configure = configure;
